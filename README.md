# WEPO Chat #
Angular and socket.io chat application, a assignment for WEPO at RU(Reykjavik University) Spring 2016.

## How do I get set up? ##
After pulling or installing the repository , you have to be sure that you've installed NodeJS and then you can run:

 1.  npm install -d
 2.  bower install

 Note: might have to run with sudo command.

## Grunt commands ##
 * To perform jshint on every .js file, run **grunt jshint**.
 * To minify/uglify every .js file, run **grunt uglify** and the output will go to client/js/dest/output.min.js.
 * To concat every .js file, run **grunt concat**.

## Run the chat app ##
 1.  Start the server, run **run-server**.
 2.  run **grunt** that will run all Grunt commands and open the chat app at localhost:8044

## Modifications to chatserver.js ##
 * When a new room is added, the roomlist updates in real time.
 * Put a setTimeout function where the room information is updated.
 * Made the operator of a room also a user when he creates a new room.

## Authors ##
Adam Haukur Baumruk (adamb14@ru.is)

Arndís Erna Björnsdóttir (arndisb14@ru.is)