angular.module("ChatApp").controller('RoomsController', ['$scope','$location','$rootScope','$routeParams','socket', '$route', '$timeout',
function ($scope, $location, $rootScope, $routeParams, socket, $route, $timeout) {
		$scope.currentUser = $routeParams.user;
		$scope.errorMessage = '';
		$scope.currentUsers = [];

		socket.emit("rooms");
		socket.on("roomlist", function(roomList) {
			$scope.rooms = Object.keys(roomList);
            $scope.roomObj = roomList;
		});

		$scope.joinRoom = function(roomID) {
			if (roomID === undefined) {
				if($scope.roomID === '' || $scope.roomID === undefined) {
					$scope.errorMessage = 'room name is empty!';
					removeError();
					return;
				} 
				else{
					$scope.currentRoom = $scope.roomID;
				}
			}
			else{
				$scope.currentRoom = roomID;
			}

			socket.emit('joinroom', {room: $scope.currentRoom, pass: ''}, function (success, reason) {
				if(!success) {
					$scope.errorMessage = 'could not join room because you got ' + reason + ' please behave yourself';
					removeError();
				}
				else{
					socket.emit("rooms");
					$location.path('/room/' + $scope.currentUser + '/' + $scope.currentRoom);
				}
			});
		};

		$scope.Disconnect = function() {
			socket.emit('disconnect');
			$location.path('/login');
		};

		function removeError () {
        	$timeout(function () {
            	$scope.errorMessage = "";
        	}, 5000); 
    	}
}]);