angular.module('ChatApp').controller('LoginController', ['$scope','$location','$rootScope','$routeParams','socket',
function ($scope, $location, $rootScope, $routeParams, socket) {
	$scope.username = '';
	$scope.errorMessage = '';

	$scope.login = function() {
		if ($scope.username === '') {
			$scope.errorMessage = 'Please choose a username';
		}
		else {
			socket.emit('adduser', $scope.username, function (available) {
				if (available) {
					$location.path('/rooms/' + $scope.username);
				}
				else {
					$scope.errorMessage = 'This username is already taken!';
				}
			});
		}
	};
}]);