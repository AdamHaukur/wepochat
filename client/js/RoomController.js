angular.module('ChatApp').controller('RoomController', ['$scope', '$routeParams', 'socket', '$location', '$route', '$timeout',
function ($scope, $routeParams, socket, $location, $route, $timeout) {
	$scope.currentRoom = $routeParams.room;
    $scope.currentUser = $routeParams.user;
	$scope.errorMsg = '';
	$scope.kickedMessage = '';
	$scope.messageHistory = [];
    $scope.roomOps = '';
    $scope.opObj = '';
    $scope.privateError = '';
    $scope.privateMsgHistory = [];

	$scope.sendMsg = function(){
		socket.emit('sendmsg', {
			roomName: $scope.currentRoom, 
			msg: $scope.newMessage
		}, function (message) {});
		$scope.newMessage = '';
	};

	socket.on('updateusers', function (roomID, users, ops) {
		if ($scope.currentRoom === roomID) {
			$scope.roomUsers = users;
            $scope.roomOps = ops;
            $scope.opObj = ops;
        }
	});

	socket.on('updatechat', function (roomID, messages) {
		if($scope.currentRoom === roomID) {
			$scope.messageHistory = messages;
		}
	});

	$scope.goBack = function() {
            $location.path('/rooms/' + $scope.currentUser);
    };

    $scope.kickedUser = function(user) {
        if(user !== $scope.currentUser){
            $scope.userKicked = user;
            socket.emit('kick', {user: user, room: $scope.currentRoom});
        }
        else{
            $scope.errorMsg = "You can't kick your self silly";
            removeError();
       }
    };

    socket.on('kicked', function(roomID, kickedUser, user) {
        if(kickedUser === $scope.currentUser) {
            $scope.goBack();
        }
        else{
            $scope.errorMsg = kickedUser + ' just got kicked from ' + roomID;
            removeError();
        }
    });

    $scope.bannedUser = function(user) {
        if(user !== $scope.currentUser){
            $scope.userBanned = user;
            socket.emit('ban', {user: user, room: $scope.currentRoom});
        }
        else{
            $scope.errorMsg = "You can't ban your self silly";
            removeError();
        }
    };

     socket.on('banned', function(roomID, bannedUser, user) {
        if(bannedUser === $scope.currentUser) {
            $scope.goBack();
        }
        else{
            $scope.errorMsg = bannedUser + ' just got banned from ' + roomID;
            removeError();
        }
    });

    $scope.sendPMsg = function(){
        if($scope.privateMsg === '') {
            $scope.privateError = 'Please write a comment!';
            removePrivateError();
        }
        else if($scope.privateMsgTo === ''){
            $scope.privateError = 'Please select a user';
            removePrivateError();
        }
        else{
            var privateMsgObj = {
                nick : $scope.privateMsgTo,
                message : $scope.privateMsg
            };
            $scope.privateMsg = '';
            socket.emit('privatemsg', privateMsgObj , function (success){
                if(!success){
                    $scope.privateError = "Cannot send message";
                    removePrivateError();
                }
            });
        }
    };

    socket.on('recv_privatemsg', function (user, message){
        if(user !== undefined && message !== undefined){
            $scope.privateMsgHistory.push({nick : user, message : message});
        }
    });

    function removeError () {
        $timeout(function () {
            $scope.errorMsg = "";
        }, 5000); 
    }

    function removePrivateError () {
        $timeout(function () {
            $scope.privateError = "";
        }, 5000); 
    }

}]);