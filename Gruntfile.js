module.exports = function ( grunt ) {
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-connect');

	grunt.initConfig({
		pgk: grunt.file.readJSON('package.json'),
		jshint: {
			src: ['client/js/*.js', '!client/js/dest/*.js'],
			gruntfile: ['Gruntfile.js'],
			options: {
				curly:  true,
				immed:  true,
				newcap: true,
				noarg:  true,
				sub:    true,
				boss:   true,
				eqnull: true,
				node:   true,
				undef:  true,
				globals: {
				    _:       false,
				    jQuery:  false,
				    angular: false,
				    moment:  false,
				    console: false,
				    $:       false,
				    io:      false
				}
			}
		},
		uglify: {
			my_target: {
				files: {
					'client/js/dest/output.min.js': ['client/js/dest/built.js']
				}
			}
		},
		concat: {
			options: {
				separator: ';'
			},
			dist: {
				src: ['node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.min.js', 'client/js/*.js', '!client/js/dest/*.js'],
				dest: 'client/js/dest/built.js'
			},
		},
		connect: {
			server: {
				options: {
					base: "client/",
					port: 8044,
					keepalive: true,
					livereload: false,
					open: true,
				}
			}
		}
	});

	grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'connect']);
	//grunt.registerTask('dev', ['jshint', 'concat', 'connect']);
};
